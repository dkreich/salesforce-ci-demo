# Salesforce-CI-Demo #

### Continuous Integration Service ###

[![Build Status](https://drone.io/bitbucket.org/dkreich/salesforce-ci-demo/status.png)](https://drone.io/bitbucket.org/dkreich/salesforce-ci-demo/latest)


[Drone IO Admin Page](https://drone.io/bitbucket.org/dkreich/salesforce-ci-demo/admin)


Code ship 


[ ![Codeship Status for dkreich/Salesforce-CI-Demo](https://codeship.com/projects/a3748280-5333-0132-fce4-5e1d8dbac3c9/status)](https://codeship.com/projects/48894)

### Description ###
This is a demo repo for Proof of Concept with the Drone.IO Continuous Integration platform.