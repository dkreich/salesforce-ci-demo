public with sharing class StringUtil {

	public static Integer countSpaces( String str ) {
		return str.split( ' ' ).size() - 1;
	}

	// this is just a comment for tracing the pull...
}